variable "cluster_name" {
  description = "The name to use for all cluster resources"
  type = string
}

variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for db state"
  type = string
}

variable "db_remote_state_key" {
  description = "path to db tf state file"
  type = string
}

variable "instance_type" {
  description = "Type of ec2 instance"
  type = string
}

variable "min_size" {
  description = "Minimum number of instances for ASG"
  type = number
}

variable "max_size" {
  description = "Maximum number of instances fro ASG"
  type = number
  
}