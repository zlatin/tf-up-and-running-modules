#!/bin/bash

cat > index.html <<EOF
<h1>echo "Hello, World!"<1h>
<p>DB address: ${db_address}
<p>DB port: ${db_port}
EOF

nohup busybox httpd -f -p ${server_port} &
