output "asg_name" {
  value       = aws_autoscaling_group.example.name
  description = "Name of autoscaling group"
}

output "alb_security_group_id" {
  description = "ID of SG attached to lb"
  value       = aws_security_group.alb.id
}
